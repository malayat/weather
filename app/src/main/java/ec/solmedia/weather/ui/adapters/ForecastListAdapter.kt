package ec.solmedia.weather.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import ec.solmedia.weather.R
import ec.solmedia.weather.domain.model.Forecast
import ec.solmedia.weather.domain.model.ForecastList
import ec.solmedia.weather.extensions.ctx
import ec.solmedia.weather.extensions.toDateString
import kotlinx.android.synthetic.main.item_forecast.view.*

class ForecastListAdapter(val weekForecast: ForecastList, val itemClick: (Forecast) -> Unit) :
        RecyclerView.Adapter<ForecastListAdapter.ForecastViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        val view = LayoutInflater.from(parent.ctx).inflate(R.layout.item_forecast, parent, false)
        return ForecastViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holderForecast: ForecastViewHolder, position: Int) {
        holderForecast.bind(weekForecast[position])
    }

    override fun getItemCount() = weekForecast.size

    class ForecastViewHolder(view: View, val itemClick: (Forecast) -> Unit) : RecyclerView.ViewHolder(view) {

        fun bind(forecast: Forecast) {
            with(forecast) {
                Picasso.with(itemView.context).load(iconUrl).into(itemView.icon)
                itemView.date.text = date.toDateString()
                itemView.description.text = description
                itemView.maxTemperature.text = "$high°"
                itemView.minTemperature.text = "$low°"
                itemView.setOnClickListener { itemClick(this) }
            }
        }
    }
}
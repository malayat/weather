package ec.solmedia.weather.domain.commans

import ec.solmedia.weather.domain.datasource.ForecastProvider
import ec.solmedia.weather.domain.model.Forecast

class RequestDayForecastCommand(
        val id: Long,
        private val forecastProvider: ForecastProvider = ForecastProvider())
    : Command<Forecast> {


    override fun execute() = forecastProvider.requestForecast(id)

}
package ec.solmedia.weather.domain.commans

import ec.solmedia.weather.domain.datasource.ForecastProvider
import ec.solmedia.weather.domain.model.ForecastList

class RequestForecastCommand(
        private val zipCode: Long,
        val forecastProvider: ForecastProvider = ForecastProvider()) : Command<ForecastList> {

    companion object {
        val DAYS = 7
    }

    override fun execute(): ForecastList {
        return forecastProvider.requestByZipCode(zipCode, DAYS)
    }
}
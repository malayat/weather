package ec.solmedia.weather.domain.datasource

import ec.solmedia.weather.domain.model.Forecast
import ec.solmedia.weather.domain.model.ForecastList


interface ForecastDataSource {

    fun requestForecastByZipCode(zipCode: Long, date: Long): ForecastList?
    fun requestDayForecast(id: Long): Forecast?
}
package ec.solmedia.weather.domain.commans

interface Command<out T> {
    fun execute(): T
}
package ec.solmedia.weather.data.db

import ec.solmedia.weather.domain.datasource.ForecastDataSource
import ec.solmedia.weather.domain.model.Forecast
import ec.solmedia.weather.domain.model.ForecastList
import ec.solmedia.weather.extensions.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class ForecastDb(
        val dbDataMapper: DbDataMapper = DbDataMapper(),
        val forecastDbHelper: ForecastDbHelper = ForecastDbHelper.instance) : ForecastDataSource {

    override fun requestForecastByZipCode(zipCode: Long, date: Long) = forecastDbHelper.use {
        val dailyRequest = "${DayForecastTable.CITY_ID} = ? AND ${DayForecastTable.DATE} >= ?"
        val dailyForecast = select(DayForecastTable.NAME)
                .whereSimple(dailyRequest, zipCode.toString(), date.toString())
                .parseList { DayForecast(HashMap(it)) }

        val cityRequest = "${CityForecastTable.ID} = ?"
        val cityForecast = select(CityForecastTable.NAME)
                .whereSimple(cityRequest, zipCode.toString())
                .parseOpt { CityForecast(HashMap(it), dailyForecast) }

        cityForecast?.let { dbDataMapper.convertToDomain(it) }
    }

    override fun requestDayForecast(id: Long): Forecast? = forecastDbHelper.use {
        val forecast = select(DayForecastTable.NAME).byId(id).parseOpt {
            DayForecast(HashMap(it))
        }

        forecast?.let { dbDataMapper.convertDayToDomain(it) }
    }

    fun saveForecast(forecastList: ForecastList) = forecastDbHelper.use {
        clear(CityForecastTable.NAME)
        clear(DayForecastTable.NAME)

        with(dbDataMapper.convertFromDomain(forecastList)) {
            insert(CityForecastTable.NAME, *map.toVarargArray())
            dayForecast.forEach { insert(DayForecastTable.NAME, *it.map.toVarargArray()) }
        }
    }


}
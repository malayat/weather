package ec.solmedia.weather.data.server

import ec.solmedia.weather.data.db.ForecastDb
import ec.solmedia.weather.domain.datasource.ForecastDataSource
import ec.solmedia.weather.domain.model.Forecast
import ec.solmedia.weather.domain.model.ForecastList


class ForecastServer(val dataMapper: ServerDataMapper = ServerDataMapper(),
                     val forecastDb: ForecastDb = ForecastDb()) : ForecastDataSource {

    override fun requestForecastByZipCode(zipCode: Long, date: Long): ForecastList? {
        val result = ForecastByZipCodeRequest(zipCode).execute();
        val converted = dataMapper.convertToDomain(zipCode, result)
        forecastDb.saveForecast(converted)
        return forecastDb.requestForecastByZipCode(zipCode, date)
    }

    override fun requestDayForecast(id: Long) = throw UnsupportedOperationException()
}
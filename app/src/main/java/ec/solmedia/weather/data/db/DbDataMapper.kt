package ec.solmedia.weather.data.db

import ec.solmedia.weather.domain.model.Forecast
import ec.solmedia.weather.domain.model.ForecastList


class DbDataMapper {

    fun convertFromDomain(forecastList: ForecastList) = with(forecastList) {
        val listDayForecast = dailyForecast.map { convertDayFromDomain(id, it) }
        CityForecast(id, city, country, listDayForecast)
    }

    private fun convertDayFromDomain(cityId: Long, forecast: Forecast) = with(forecast) {
        DayForecast(date, description, high, low, iconUrl, cityId)
    }

    fun convertToDomain(cityForecast: CityForecast) = with(cityForecast) {
        val listDailyForecast = dayForecast.map { convertDayToDomain(it) }
        ForecastList(_id, city, country, listDailyForecast)
    }


    fun convertDayToDomain(dayForecast: DayForecast) = with(dayForecast) {
        Forecast(_id, date, description, high, low, iconUrl)
    }
}